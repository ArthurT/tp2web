var prix = 0;

function valider(){
	prix = 0;
	if(document.getElementById('platdj').checked){
		prix = 10;
		alert("Merci d'avoir choisi notre plat du jour");
	}else if (document.getElementById('menu').checked) {
		alert("Régalez vous avec notre menu ci-dessous");
	}
	document.getElementById('prix').value = prix;
}

function payer(){
	var tab = ['entre', 'plat', 'dessert'];
	for(var i in tab){
		prix += Number(document.getElementById(tab[i]).value);
	}
	document.getElementById('prix').value = prix;
}
